const accordionItems = document.querySelectorAll('.accordion__button')

function toggleAccordion() {
  const itemToggle = this.getAttribute('data-area-activeted')

  for (let i = 0; i < accordionItems.length; i++) {
    accordionItems[i].setAttribute('data-area-activeted', 'false')
  }

  if (itemToggle == 'false') {
    this.setAttribute('data-area-activeted', 'true')
  }
}

accordionItems.forEach((item) => item.addEventListener('click', toggleAccordion))
