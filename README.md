# Сборка для верстки

## Команды установок

`$ npm init -y` _дефолтный package, без заполнения_

`$ git flow init` _установка git-flow_

---

## Prettier

**через консоль:**

`npx prettier --write "."` - отформатировать файлы в текущей директории "_path_"

`npx prettier --check "."` - проверяет файлы на форматирование в текущей директории "_path_"

**Редактор Webstorm:**

`Ctrl + Alt + Shift + P` - комбинация клавиш

**Редактор VS Code:**

`Alt + Shift + F` - комбинация клавиш

### Игнорирование кода

Комментарий _prettier-ignore_ исключит следующий узел в абстрактном синтаксическом дереве из форматирования.

JS

`// prettier-ignore`

CSS

`/* prettier-ignore */`

HTML

`<!-- prettier-ignore -->`

---

Макет по верстке **Appello** :

[Appello](https://www.figma.com/file/7ggMY0FwFRSbXzQ6rCTGet/Appello-Copy)

![Tux, the Linux mascot](src/assets/Desktop.png)
